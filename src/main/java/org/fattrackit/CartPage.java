package org.fattrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");
    private final SelenideElement productDescription = $ (".row");
    private final SelenideElement continueShoppingButton = $(".btn-danger");
    private final SelenideElement cleanButton = $ (".fa-trash");
    private final SelenideElement checkoutButton = $ (".btn-success");
    private final SelenideElement checkoutPageInfo = $ (".text-muted");
    private final SelenideElement cancelButton = $ (".btn-danger");
    private final SelenideElement textCart = $ (".text-muted");



    public CartPage() {
    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMassageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }

    public String setProductDescription() {
        return this.productDescription.text();
    }
    public void clickOnTheContinueShoppingButton () {
        continueShoppingButton.click();
    }

    public void clickOnTheCleanButton () {
        cleanButton.click();
    }
    public void  clickOnTheCheckoutButton () {
        checkoutButton.click();
    }

    public String getCheckoutPageInfo () {
        return this.checkoutPageInfo.text();
    }
    public void clickOnTheCancelButton () {
        cancelButton.click();
    }
    public String getCartMassage (){
        return this.textCart.text();
    }
}

