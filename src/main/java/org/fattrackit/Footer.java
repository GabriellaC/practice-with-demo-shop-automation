package org.fattrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement resetStateButton = $(".fa-undo");
    private final SelenideElement helpButton = $ (".fa-question");

    @Step("Reset app state.")
    public void clickToReset() {
        resetStateButton.click();
    }
    @Step
    public void  clickToHelp () {
        helpButton.click();
    }
}
