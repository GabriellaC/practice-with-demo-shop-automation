package org.fattrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishListButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement xButton = $(".close");


    @Step("Click on the Login button.")
    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }

    public String getGreetingsMessage() {
        return greetingElement.text();
    }

    public void clickOnTheWishListIcon() {
        System.out.println("Click on the Wishlist button.");
        wishListButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon.");
        cartIcon.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }
    public boolean isShoppingBadgeVisible(){
        return !this.shoppingCartBadges.isEmpty();

    }

    public void clickOnTheXButton () {
        System.out.println("Click on the X button.");
        xButton.click();
    }


}
