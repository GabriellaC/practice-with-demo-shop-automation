package org.fattrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class InvalidAccount extends Account {
    private final String errorMsg;


    public InvalidAccount(String user, String password, String errorMsg) {
        super(user, password);
        this.errorMsg = errorMsg;
    }


    public String getErrorMsg() {
        return errorMsg;
    }

}
