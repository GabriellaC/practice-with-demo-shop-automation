package org.fattrackit;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement close = $(".close");
    private final SelenideElement errorElement = $(".error");
    private final SelenideElement logoutButton = $ (".fa-sign-out-alt");
    private final SelenideElement getGreetingMessage = $(".navbar-text span");


    @Step("Type in username.")
    public void typeInUsername(String user) {
        System.out.println("Click on the Username filed.");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }
    @Step("Type in password.")
    public void typeInPassword(String pass) {
        System.out.println("Click on the Password Field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }
    @Step("Click on the login button.")
    public void clickOnTheLoginButton() {
        System.out.println("Click on the Login button.");
        loginButton.click();
    }
    public boolean isErrorMsgDisplayed() {
        return this.errorElement.isDisplayed();
    }

    public String getErrorMsg() {
        return this.errorElement.text();
    }

    public void close() {
        this.close.click();
    }

    public void clickOnTheLogoutButton () {
        System.out.println("Click on the Logout button.");
        logoutButton.click();
    }
    public String getMessage() {
        return this.getGreetingMessage.text();
    }
}


