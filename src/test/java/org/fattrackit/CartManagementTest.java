package org.fattrackit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMassageDisplayed(), "After adding one product to cart, empty cart message is not shown.");
    }

    @Test
    public void user_can_increment_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.increaseAmount();
        assertEquals(item.getItemAmount(),"2", "The products can be incremented.");
    }

    @Test
    public void user_can_reduce_the_amount_of_a_product_in_cart_page() {
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem("9");
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1", "The products can be reduced.");
    }

    @Test
    public void user_can_continue_shopping_after_adding_products_to_the_cart() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "After press continue shopping button, expected greetings message to be Hello guest!");
    }

    @Test
    public void user_dino_can_continue_shopping_after_adding_products_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(header.getGreetingsMessage(), "Hi dino!", "After press continue shopping button, expected greetings message to be Hi dino!");
    }

    @Test
    public void user_turtle_can_continue_shopping_after_adding_products_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(header.getGreetingsMessage(), "Hi turtle!", "After press continue shopping button, expected greetings message to be Hi turtle!");
    }

    @Test
    public void user_can_delete_products_to_the_cart() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCleanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?", "After press clean button, expected greetings message to be How about adding some products in your cart?");
    }
    @Test
    public void user_dino_can_delete_products_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCleanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?", "After press clean button, expected greetings message to be How about adding some products in your cart?");
    }

    @Test
    public void user_turtle_can_delete_products_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCleanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?", "After press clean button, expected greetings message to be How about adding some products in your cart?");
    }

    @Test
    public void user_can_checkout_to_the_cart() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertEquals(cartPage.getCheckoutPageInfo(), "Your information", "After press checkout button, expected greetings message to be Your information");
    }
    @Test
    public void user_dino_can_checkout_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertEquals(cartPage.getCheckoutPageInfo(), "Your information", "After press checkout button, expected greetings message to be Your information");
    }
    @Test
    public void user_turtle_can_checkout_to_the_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertEquals(cartPage.getCheckoutPageInfo(), "Your information", "After press checkout button, expected greetings message to be Your information");
    }
    @Test
    public void user_can_return_to_the_cart_page_from_checkout_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        cartPage.clickOnTheCancelButton();
        assertEquals(cartPage.getCartMassage(), "Your cart", "After press cancel button, expected greetings message to be Your cart");
    }
    @Test
    public void user_dino_can_return_to_the_cart_page_from_checkout_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        cartPage.clickOnTheCancelButton();
        assertEquals(cartPage.getCartMassage(), "Your cart", "After press cancel button, expected greetings message to be Your cart");
    }
    @Test
    public void user_turtle_can_return_to_the_cart_page_from_checkout_page() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        cartPage.clickOnTheCancelButton();
        assertEquals(cartPage.getCartMassage(), "Your cart", "After press cancel button, expected greetings message to be Your cart");
    }
}
